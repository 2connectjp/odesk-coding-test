# Odesk Coding Test
You, as a developer, need to slice a design and code a small backend using PHP.

## Requirements 
The home page can be static, but for the product page you need to do 2 things.

1. Write a Product model
2. Use this model to inject products into the view.

You do not need to provide any database functionality.

## Limitations
You can only use the following frameworks:

- Codeigniter
- jQuery

Any other frameworks are not acceptable.

## Getting Started

Fork this repository, and create a new branch with your BitBucket username. When sending a pull request send them on this branch. Do not send pull requests to any other forks.

If you have any questions contact info@2connect.jp
